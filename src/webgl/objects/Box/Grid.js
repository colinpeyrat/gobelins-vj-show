import { Group, Box3 } from 'three';
import Box from './Box';
import TweenMax from 'gsap/TweenMax';
import { getViewport } from '../../../utils/Screen';
import Sound from '../../../utils/Sound';

class Grid extends Group {
  constructor() {
    super();

    this.name = 'box';
    this.stepScale = 1;
    this.scaleDirection = 'down';
    this.scaleMax = 3;
    this.hasZoomed = false;

    const { width, height } = getViewport();

    const rows = 9;
    const cols = 16;

    const offset = {
      x: 2.5,
      y: 5
    };

    const cell = {
      width: width / cols - offset.x,
      height: height / rows - offset.y
    };

    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        const initialScale = Math.sin(j + i) * 0.25 + 0.75;
        const box = new Box({ initialScale });
        box.position.set(j * cell.width, i * cell.height, 1);
        this.add(box);
      }
    }

    this.center();

    this.scale.set(this.scaleMax, this.scaleMax, this.scaleMax);
  }

  update() {
    const value = Sound.getAverageAmplitude();

    const threshold = 0.65;

    if (value > threshold) {
      this.onPeak();
    }
  }

  onPeak() {
    this.zoomIn();
  }

  zoomIn() {
    if (this.hasZoomed) {
      return;
    }

    let scale;
    if (this.scaleDirection == 'up') {
      if (this.scale.x < this.scaleMax) {
        scale = this.scale.x + this.stepScale;
      } else {
        this.scaleDirection = 'down';
        scale = this.scale.x - this.stepScale;
      }
    } else {
      if (this.scale.x > 1) {
        scale = this.scale.x - this.stepScale;
      } else {
        this.scaleDirection = 'up';
        scale = this.scale.x + this.stepScale;
      }
    }

    this.scale.set(scale, scale, scale);

    this.hasZoomed = true;
    TweenMax.delayedCall(0.1, () => {
      this.hasZoomed = false;
    });
  }

  center() {
    const box = new Box3().setFromObject(this);
    const { min, max } = box;
    const offsetX = -((max.x + min.x) / 2);
    const offsetY = -((max.y + min.y) / 2);

    this.children.forEach(child => {
      child.position.x += offsetX;
      child.position.y += offsetY;
    });
  }
}

export default Grid;
