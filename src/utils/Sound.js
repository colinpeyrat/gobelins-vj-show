import sono from 'sono';
import 'sono/effects';

class Sound {
  constructor() {
    this.sound = sono
      .create({
        url: ['static/sounds/playlist.mp3'],
        loop: true
      })
      .play();

    this.analyse = this.sound.effects.add(
      sono.analyser({
        fftSize: 256,
        smoothing: 0.7
      })
    );
  }

  getFrequencies() {
    return this.analyse.getFrequencies();
  }

  averageAmplitude(wave) {
    let sum = 0;
    for (let i = 0; i < wave.length; i++) {
      sum += wave[i];
    }

    return sum / wave.length / 256;
  }

  getAverageAmplitude() {
    return this.averageAmplitude(this.analyse.getWaveform());
  }
}

export default new Sound();
