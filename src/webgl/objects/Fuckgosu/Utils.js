/**
 * Created by Sko on 01/06/2018.
 */

const Utils = {
  norm: function (value, min, max) {
    return (value - min) / (max - min);
  },

  lerp: function (norm, min, max) {
    return (max - min) * norm + min;
  },

  map: function (value, sourceMin, sourceMax, destMin, destMax) {
    return Utils.lerp(Utils.norm(value, sourceMin, sourceMax), destMin, destMax);
  },

  clamp: function (value, min, max) {
    return Math.min(Math.max(value, Math.min(min, max)), Math.max(min, max));
  }
};

export default Utils;
