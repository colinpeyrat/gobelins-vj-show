import glslify from 'glslify';
import vertexShaderSource from './glow.vert';
import fragmentShaderSource from './glow.frag';

// TODO npm script to automate the creation of new shaders

const vertexShader = glslify(vertexShaderSource);
const fragmentShader = glslify(fragmentShaderSource);

export { vertexShader, fragmentShader };
