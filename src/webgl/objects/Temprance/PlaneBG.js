import {
  PlaneBufferGeometry,
  Mesh,
  Object3D,
  TextureLoader,
  RepeatWrapping
} from 'three';
import { getViewport } from '../../../utils/Screen';

class PlaneBG extends Object3D {
  constructor() {
    super();

    let ShaderFrogRuntime = require('shaderfrog-runtime');
    let runtime = new ShaderFrogRuntime();

    runtime.load('/static/textures/shader_4.json', shaderData => {
      this.material = runtime.get(shaderData.name);

      let loader = new TextureLoader();

      loader.load('/static/textures/mosaic.jpg', texture => {
        texture.wrapS = RepeatWrapping;
        texture.wrapT = RepeatWrapping;
        this.material.uniforms.texture.value = texture;
        const { width, height } = getViewport();
        let geometry = new PlaneBufferGeometry(width, height);
        this.mesh = new Mesh(geometry, this.material);
        this.add(this.mesh);
      });
    });
  }

  update(dt = 0, time = 0) {
    if (this.material) {
      this.material.uniforms.time.value = time;
      // this.material.uniforms.width.value = (Math.cos(time) * 0.5 + 0.5) * 20 + 70;
    }
  }
}

export default PlaneBG;
